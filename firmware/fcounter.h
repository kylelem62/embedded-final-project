#ifndef FCOUNTER
#define FCOUNTER value

/*
  The frequency counter module monitors the input PD7, compared to an internal
  2.56v reference voltage. The measurement of the timer1 value is performed
  on the rising edge of a signal on PD7, when the signal rises above 2.56v,
  and the analog comparator triggers the Input Capture interrupt for timer1.

  Despite timer1 only supporting a 16 bit counter, the timer1 overflow interrupt
  is caught, and a variable is incremented to count up from 0, effectively
  increasing the 16 bit counter to a 24 bit counter. This value is then
  bit-shifted and added to the value from timer1 when the input compare is
  triggered. For this reason, the average value is a 32 bit integer, because
  frequencies that are too low will result in periods that are longer than
  the maximum 65535 clock cycles.
*/

//Initialize the fcounter, including interrupts
void fcounter_init(void);
//Get the average period from the last 128 period time measurements
//This function will be slow!
uint32_t get_avg_period(void);

#endif
