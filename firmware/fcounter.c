#include <avr/io.h>
#include <avr/interrupt.h>
#include "fcounter.h"
#include "lcd_1602.h"

//IMPORTANT! Should be a power of 2, or performance will be impacted
#define VAL_CNT 128

//IMPORTANT! Should be a power of 2, or performance will be impacted
#define AVG_CNT 64

//store the timer values
uint32_t lastTimer[VAL_CNT] = {};

uint16_t avgs[AVG_CNT] = {};
uint8_t tcc = 0, iavg = 0, of_cnt = 0;

void fcounter_init(){
  //PD7 as input
  DDRD &= ~(1<<7);
  //PD7 with no pullup
  PORTD &= ~(1<<7);

  //Disable comparator, use fixed bandgap reference,
  ACSR = (1 << 6) | (1 << 2) | (3 << 0);

  //Ensure no compare settings are set on timer1
  TCCR1A = 0;
  //Set clock source no prescaling
  TCCR1B |= (1<<0) | (1 << 6) | (1 << ICNC1);
  //Reset interrupt flags - just in case
  TIFR = (1 << ICF1) | (1 << TOV1);
  //Enable timer/counter input capture interrupt, and overflow
  TIMSK |= (1 << TICIE1) | (1 << TOIE1);

}

uint16_t get_previous_val(){
  if(tcc > 0);
    return lastTimer[tcc-1];
  return lastTimer[9];
}

uint32_t get_avg_period(){
  //accumulator
  uint32_t avg = 0;
  uint8_t i = 0;
  //Loop through the saved timer values
  for(i=0;i<VAL_CNT;i++){
    avg += lastTimer[i];
  }
  //should be optimized as bitshift!
  avg = avg / VAL_CNT;
  //return avg
  return (uint32_t)avg;
}

uint16_t get_avg_val_small(){
  uint32_t avg = 0;
  uint8_t i = 0;
  for(i=0;i<VAL_CNT;i++){
    avg += lastTimer[i];
  }
  avg = avg / VAL_CNT;
  return (uint16_t)avg;
}

//Timer1 input capture interrupt
ISR(TIMER1_CAPT_vect){
  //ICR1 is the timer value when the capture was triggered, not current timer
  //Bit shift the of_cnt to take care of any timer overflows that may have
  //happened
  lastTimer[tcc++] = ICR1 + (((uint32_t)of_cnt) << 15);
  //reset overflow counter
  of_cnt = 0;
  //reset timer1
  TCNT1 = 0;
  //Check to make sure the incrementer is still valid
  if(tcc >= VAL_CNT){
    //Reset it if it is out of bounds
    tcc = 0;
  }
}

//Timer1 overflow interrupt
ISR(TIMER1_OVF_vect){
  //Increment the overflow counter
  of_cnt++;
}
