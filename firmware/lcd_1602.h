#include <avr/io.h>
#ifndef LCD_1602
#define LCD_1602 true
/*
  The LCD module is a software implementation of the documentation provided.
  Unfortunately, the 1602 LCD modules do not implement a standard protocol that
  is supported at the hardware level, and in keeping with the assignment
  difficulty requirements, I have implemented the protocol here. The
  documentation is included in the submission.

  As a note, each one of these functions will introduce the necessary delays
  using the AVR delay macros. The required delay between calls of these
  functions is not implemented. That was a consious decision in order to allow a
  function to be called and work to be done immediately afterwards, without
  unnecessary delays. So, the delays must be implemented where the functions are
  called successively. The required delays are noted in the documentation for
  each function.
*/

// Don't change any settings, but delete all characters on the LCD
void lcd_clear(void);
// Write a string to the display
void lcd_write(char*);
// Write a string from program memory to the display
void lcd_write_pgm(char*);
// Write a single character to the display
void lcd_write_char(char);
// Write a number to the display
void lcd_write_num(uint32_t);
// Turn the lcd on
void lcd_on(void);
// Turn the lcd off`
void lcd_off(void);
// Initialize the LCD module
void lcd_initio(void);

#endif
