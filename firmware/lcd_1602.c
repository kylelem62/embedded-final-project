
#include "lcd_1602.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

//Some macros that make configuration easy
#define GETPORT(x) PORT##x
#define GETDDR(x) DDR##x

//Port of 6 contiguous low data bits
#define DPORT6L GETPORT(D)
//Data direction register of 6 low bits
#define DDDR6L GETDDR(D)
//Pin offset of 6 low bits
#define DPORT6L_OFFSET 0

//Port of 2 high data bits
#define DPORT2H GETPORT(B)
//Data direction register of 2 high bits
#define DDDR2H GETDDR(B)
//Pin offset of 2 high bits
#define DPORT2H_OFFSET 0

//RS port
#define RSPORT GETPORT(B)
//RS data direction register
#define RSDDR GETDDR(B)
//RS pin offset
#define RSBIT 2

//RW port
#define RWPORT GETPORT(B)
//RW data direction register
#define RWDDR GETDDR(B)
//RW pin offset
#define RWBIT 3

//Enable port
#define EPORT GETPORT(B)
//Enable data direction register
#define EDDR GETDDR(B)
//Enable pin offset
#define EBIT 4

void lcd_initio(){
  //configure outputs
  DDDR6L |= (0x3F << DPORT6L_OFFSET);
  DDDR2H |= (0x03 << DPORT2H_OFFSET);
  RSDDR |= (0x01 << RSBIT);
  RWDDR |= (0x01 << RWBIT);
  EDDR |= (0x01 << EBIT);
}

void lcd_set_data(char dat){
  //Reset data to zeros
  DPORT6L &= ~(0x3F << DPORT6L_OFFSET);
  DPORT2H &= ~(0x03 << DPORT2H_OFFSET);
  //Set data to argument value
  DPORT6L |= ((0x3F & dat) << DPORT6L_OFFSET);
  DPORT2H |= ((0x03 & (dat>>6)) << DPORT2H_OFFSET);
}

void lcd_write_num(uint32_t val){
  char str[16];
  //unsigned long for 32 bit unsigned int
  ultoa(val,str,10);
  //Write the number string to lcd
  lcd_write(str);
}

void lcd_inst(char inst){
  //clear rs
  RSPORT &= ~(1<<RSBIT);
  //clear rw
  RWPORT &= ~(1<<RWBIT);
  //Set the data bits as per 1602 documentation
  lcd_set_data(inst);
  //Set the enable bit
  EPORT |= (1<<EBIT);
  //Wait required delay per documentation
  _delay_us(0.15);
  //Latch
  EPORT &= ~(1<<EBIT);

}

void lcd_set_ddram(char addr){
  //Mask address by instruction info per documentation
  lcd_inst((addr & 0x7F) | 0x80);
}

void lcd_write_char(char chr){
  //set the data bits of the character to write
  lcd_set_data(chr);
  //set rs
  RSPORT |= (1<<RSBIT);
  //clear rw
  RWPORT &= ~(1<<RWBIT);
  //Enable writing
  EPORT |= (1<<EBIT);
  //Wait required delay
  _delay_us(0.15);
  //Latch
  EPORT &= ~(1<<EBIT);
}

void lcd_write_pgm(char* str){
  //Loop while not at the end of the string
  while(pgm_read_byte(str) != '\0'){
    //write a single character using program memory read macro
    lcd_write_char(pgm_read_byte(str));
    //wait required delay
    _delay_us(100);
    //go on to the next character
    str++;
  }
}

void lcd_write(char* str){
  //loop while not at the end of the string
  while(*str != '\0'){
    //write the character and increment
    lcd_write_char(*(str++));
    //wait required delay
    _delay_us(100);
  }
}

void lcd_off(){
  //Off command byte as per documentation
  lcd_inst(0x08);
}

void lcd_on(){

  //Display on command per documentation
  //Note that I am disabling the cursor and cursor underline
  lcd_inst(0x0C);
  _delay_ms(3);
  //set lcd function for 8 bit operation and 2 line text
  lcd_inst(0x35);
}

void lcd_clear(){
  //send the clear command
  lcd_inst(0x01);
}
