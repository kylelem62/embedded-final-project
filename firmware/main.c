#define F_CPU 16000000  // CPU frequency for proper time calculation in delay function

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "lcd_1602.h"
#include "fcounter.h"
#include <avr/interrupt.h>

const __flash uint8_t cap[] = "C";
const __flash uint8_t ind[] = "L";
const __flash uint8_t frq[] = "F";

const __flash char* const __flash modes[] =
  {
    cap,
    ind,
    frq
  };

const __flash uint8_t capu[] = "F";
const __flash uint8_t indu[] = "H";
const __flash uint8_t frqu[] = "kHz";

const __flash char* const __flash modeunits[] =
  {
    capu,
    indu,
    frqu
  };

uint8_t debounce = 0;
uint8_t mode = 0;

void init_button(void){
  //PD6 as input
  DDRD |= (1<<6);
  //Pull-up enabled
  PORTD |= (1<<6);
}

uint8_t check_button_press(void){
  //Check PD6 if low
  return !(PIND & (1 << 6));
}

int main(void)
{
  //Initialize the IO components
  lcd_initio();
  fcounter_init();
  //For some reason, power-cycling the LCD makes it more consistent on startup
  lcd_off();
  _delay_ms(5);
  lcd_on();
  _delay_ms(5);

  //enable interrupts globally
  sei();

  while(1)
  {
    if(check_button_press()){
      if(!debounce){
        mode++;
        if(mode > 2){
          mode = 0;
        }
        debounce = 1;
      }
    }else{
      debounce = 0;
    }

    lcd_write_pgm(modes[mode]);
    //Get the average period over the last 128 cycles
    uint32_t val = get_avg_period();
    //calculate the frequency from the average period
    uint64_t freq = (uint64_t)(1.0d/(1.0d/F_CPU*val));
    _delay_ms(2);

    lcd_set_ddram(0x05);
    _delay_ms(2);

    //write the frequency to the LCD
    lcd_write_num(freq);

    _delay_ms(2);

    lcd_set_ddram(0x0d);
    _delay_ms(2);
    lcd_write_pgm(modeunits[mode]);
    _delay_ms(200);

    lcd_clear();
    _delay_ms(1.6);
  }
  //So the compiler doesn't complain
  return 0;
}
