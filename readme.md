# LC Meter Project

The purpose of this project was to create an accurate inductance and capacitance
meter that could measure most values of common inductors and capacitors. In
addition, the device is also capable of measuring frequencies, so it is also
a frequency counter.

## Firmware

The firmware is split into three sections. The main process loop, which
calculates and updates the display with the measured values, the LCD module,
which is a c software implementation of the 1602 character LCD protocol, and the
frequency counter module, which counts the frequency of an incoming signal.

### Main Process

The main process loop initializes the other two modules, and begins polling the
frequency counter, the mode selection, and updating the LCD with the appropriate
values.

### LCD Module

The LCD is a standard 16x2 character display, and I was able to find a
datasheet, which is included in this repo. I used the datasheet to create a
software implementation of the protocol to control the LCD. The LCD module uses
no interrupts and no hardware peripherals, purely relying on software delays and
GPIO. I chose to implement this without interrupts because the required delays
are very short, only a couple of microseconds.

### Frequency Counter

The frequency counter is an interrupt-driven timer sampling module. The
principle behind my implementation of frequency counting is simply to measure
the number of clock cycles that pass during a complete cycle of the waveform
being measured. The analog comparator is configured to trigger the input capture
pin of the timer/counter1 hardware peripheral, which also triggers an input
capture interrupt. The input capture interrupt reads and pushes the value of the
timer when the capture was started to a rotating list of the last 128
measurements. When the main process read the average value, these 128 values
are averaged to eliminate noise.

## Hardware
In the circuit folder you can see my plans for the hardware implementation.
Unfortunately, I was not able to get the oscillator producing a clean enough
waveform to produce accurate results when read by the AVR. The frequency was
stable and I calculated it to be close to the expected value when I switched
capacitors or inductors, but the wave crossed the center point more than once
per cycle.
